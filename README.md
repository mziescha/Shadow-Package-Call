# Shadow-Package-Call

``
$> prove -Ilib
$> t/shadow_call_require_ok.t ... ok   
$> t/shadow_call_require_ok2.t .. ok   
$> t/shadow_call_require_ok3.t .. ok   
$> t/shadow_call_use_fails.t .... ok   
$> t/shadow_call_use_ok.t ....... ok   
$> t/shadow_call_use_ok2.t ...... ok   
$> All tests successful.
$> Files=6, Tests=26,  0 wallclock secs ( 0.04 usr  0.02 sys +  0.25 cusr  0.07 csys =  0.38 CPU)
$> Result: PASS
``