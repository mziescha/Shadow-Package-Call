package Acme::MZIESCHA::Tests;


sub new {
   return bless {}, shift;
}

sub Compute {
   return __PACKAGE__ . '::Compute ' . __LINE__;
}

sub AnotherCompute {
   return {__PACKAGE__ . '::AnotherCompute ' => __LINE__};
}

1;

__END__
