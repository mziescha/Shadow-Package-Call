package Acme::MZIESCHA::Tests::Compute;

sub new {
    return bless {}, shift;
}

sub foo_bar {
    return __PACKAGE__ . '::foo_bar ' . __LINE__;
}

1;

__END__
