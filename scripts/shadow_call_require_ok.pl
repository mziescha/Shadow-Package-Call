#!/usr/bin/env perl

use DDP;
use utf8;
use strict;
use warnings;
use FindBin qw/$Bin/;

use lib $Bin. '/../lib';

sub main {
    my ($s_another) = @_;
    my $test;
    # first step it works all well
    if ($s_another){
        require Acme::MZIESCHA::Tests::AnotherCompute;
        $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar();
    }else{
        require Acme::MZIESCHA::Tests::Compute;
        $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar();
    }
    p $test;
    
    require Acme::MZIESCHA::Tests;
    if ($s_another){
        $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar();
    }else{
        $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar();
    }
    p $test;
}

main(@ARGV);

exit 0;

__END__
