#!/usr/bin/env perl

use DDP;
use utf8;
use strict;
use warnings;
use FindBin qw/$Bin/;

use lib $Bin. '/../lib';

sub main {
    my ($s_another) = @_;
    require Acme::MZIESCHA::Tests;
    require Acme::MZIESCHA::Tests::Compute;
    require Acme::MZIESCHA::Tests::AnotherCompute;

    my $test;
    # first step it works all well
    if ($s_another){
        $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar();
    }else{
        $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar();
    }
    p $test;
}

main(@ARGV);

exit 0;

__END__
