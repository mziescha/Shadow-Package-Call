#!/usr/bin/env perl

use DDP;
use utf8;
use strict;
use warnings;
use FindBin qw/$Bin/;

use lib $Bin. '/../lib';

use Acme::MZIESCHA::Tests;
use Acme::MZIESCHA::Tests::Compute;
use Acme::MZIESCHA::Tests::AnotherCompute;

sub main {
    my ($s_another) = @_;
    my $test;
    if ($s_another){
        # Shadow call through:
        # package Acme::MZIESCHA::Tests
        # sub AnotherCompute
        #
        # ERROR:
        # Can't call method "new" on unblessed reference at ./scripts/strange_static_call.pl line 20.
        $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar();
    }else{
        # Shadow call through:
        # package Acme::MZIESCHA::Tests
        # sub Compute
        #
        # ERROR:
        # Can't locate object method "new" via package "Acme::MZIESCHA::Tests::Compute 9" (perhaps you forgot to load "Acme::MZIESCHA::Tests::Compute 9"?) at ./scripts/shadow_call_use_fails.pl line 21.
        $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar();
    }
    p $test;
}

main(@ARGV);

exit 0;

__END__
