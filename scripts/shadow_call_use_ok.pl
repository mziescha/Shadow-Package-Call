#!/usr/bin/env perl

use DDP;
use utf8;
use strict;
use warnings;
use FindBin qw/$Bin/;

use lib $Bin. '/../lib';

use Acme::MZIESCHA::Tests::Compute;
use Acme::MZIESCHA::Tests::AnotherCompute;

sub main {
    my ($s_another) = @_;
    my $test;
    if ($s_another){
        $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar();
    }else{
        $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar();
    }
    p $test;
}

main(@ARGV);

exit 0;

__END__
