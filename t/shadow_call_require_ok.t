#!/usr/bin/env perl

use utf8;
use strict;
use warnings;
use Test::More tests => 7;

my $test;

# first step it works all well
require_ok 'Acme::MZIESCHA::Tests::AnotherCompute';
ok $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar(), 'run after require AnotherCompute ok';
require_ok 'Acme::MZIESCHA::Tests::Compute';
ok $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar(), 'run after require AnotherCompute ok';

require_ok 'Acme::MZIESCHA::Tests';
ok $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar(), 'run after require AnotherCompute ok';
ok $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar(),        'run after require AnotherCompute ok';

exit 0;

__END__
