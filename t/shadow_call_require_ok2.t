#!/usr/bin/env perl

use utf8;
use strict;
use warnings;
use Test::More tests => 5;


my $test;
{
   require_ok 'Acme::MZIESCHA::Tests';
   require_ok 'Acme::MZIESCHA::Tests::Compute';
   require_ok 'Acme::MZIESCHA::Tests::AnotherCompute';

# first step it works all well
   ok $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar(),
      'run AnotherCompute->new after full require in block ok';
   ok $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar(),
      'run Compute->new after full require in block ok';
}
exit 0;

__END__
