#!/usr/bin/env perl

use utf8;
use strict;
use warnings;
use Test::More tests => 2;
use Test::Exception;

use Acme::MZIESCHA::Tests;
use Acme::MZIESCHA::Tests::Compute;
use Acme::MZIESCHA::Tests::AnotherCompute;

# Shadow call through:
# package Acme::MZIESCHA::Tests
# sub AnotherCompute
#
# ERROR:
# Can't call method "new" on unblessed reference at ./scripts/strange_static_call.pl line 20.
dies_ok sub { Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar() }, '';

# Shadow call through:
# package Acme::MZIESCHA::Tests
# sub Compute
#
# ERROR:
# Can't locate object method "new" via package "Acme::MZIESCHA::Tests::Compute 9" (perhaps you forgot to load "Acme::MZIESCHA::Tests::Compute 9"?) at ./scripts/shadow_call_use_fails.pl line 21.
dies_ok sub { Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar() }, '';

exit 0;

__END__
