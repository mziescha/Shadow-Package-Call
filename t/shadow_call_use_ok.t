#!/usr/bin/env perl

use utf8;
use strict;
use warnings;
use Test::More tests => 5;
use Test::Exception;

use_ok 'Acme::MZIESCHA::Tests';
use_ok 'Acme::MZIESCHA::Tests::Compute';
use_ok 'Acme::MZIESCHA::Tests::AnotherCompute';

# it works because of use_ok and not use only
is Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar(),
   'Acme::MZIESCHA::Tests::AnotherCompute::foo_bar 8';

# it works because of use_ok and not use only
is Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar(), 'Acme::MZIESCHA::Tests::Compute::foo_bar 8';

exit 0;

__END__
