#!/usr/bin/env perl

use utf8;
use strict;
use warnings;
use Test::More tests => 2;

use Acme::MZIESCHA::Tests::Compute;
use Acme::MZIESCHA::Tests::AnotherCompute;

my $test;
ok $test = Acme::MZIESCHA::Tests::AnotherCompute->new(@_)->foo_bar(),
   'run AnotherCompute->new(@_)->foo_bar() on not include Acme::MZIESCHA::Tests';
ok $test = Acme::MZIESCHA::Tests::Compute->new(@_)->foo_bar(),
   'run Compute->new(@_)->foo_bar() on not include Acme::MZIESCHA::Tests';

exit 0;

__END__
